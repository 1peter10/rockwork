# Portuguese translation for upebble
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the upebble package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: upebble\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-08-21 05:09+0000\n"
"PO-Revision-Date: 2016-02-26 15:02+0000\n"
"Last-Translator: Vitor Loureiro <Unknown>\n"
"Language-Team: Portuguese <pt@li.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2016-06-14 06:29+0000\n"
"X-Generator: Launchpad (build 18097)\n"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppSettingsPage.qml:16
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppSettingsPage.qml:14
msgid "App Settings"
msgstr "Definições"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppSettingsPage.qml:136
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppSettingsPage.qml:53
msgid "Cut"
msgstr "Cortar"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppSettingsPage.qml:148
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppSettingsPage.qml:59
msgid "Copy"
msgstr "Copiar"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppSettingsPage.qml:160
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppSettingsPage.qml:65
msgid "Paste"
msgstr "Colar"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppSettingsPage.qml:173
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:197
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:196
msgid "Close"
msgstr "Fechar"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:9
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:9
msgid "App details"
msgstr "Detalhes"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:57
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:57
msgid "Install"
msgstr "Instalar"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:57
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:57
msgid "Installing..."
msgstr "A instalar..."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:57
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:57
msgid "Installed"
msgstr "Instalado"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:235
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:235
msgid "Description"
msgstr "Descrição"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:258
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:258
msgid "Developer"
msgstr "Programador"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStoreDetailsPage.qml:266
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStoreDetailsPage.qml:266
msgid "Version"
msgstr "Versão"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStorePage.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStorePage.qml:8
msgid "Add new watchapp"
msgstr "Adicionar nova watchapp"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStorePage.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStorePage.qml:8
msgid "Add new watchface"
msgstr "Adicionar nova watchface"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/AppStorePage.qml:125
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/AppStorePage.qml:125
msgid "See all"
msgstr "Ver todos"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DailySleepGraph.qml:127
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DailySleepGraph.qml:127
msgid "Oops! No sleep data."
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DailyStepsGraph.qml:118
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DailyStepsGraph.qml:118
msgid "TODAY'S STEPS"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DailyStepsGraph.qml:120
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DailyStepsGraph.qml:120
msgid "YESTERDAY'S STEPS"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DailyStepsGraph.qml:122
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DailyStepsGraph.qml:122
msgid "STEPS"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DailyStepsGraph.qml:125
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DailyStepsGraph.qml:125
msgid "TYPICAL"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DeveloperToolsPage.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DeveloperToolsPage.qml:8
msgid "Developer Tools"
msgstr "Ferramentas de Desenvolvimento"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DeveloperToolsPage.qml:27
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:11
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DeveloperToolsPage.qml:27
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:11
msgid "Screenshots"
msgstr "Capturas de ecrã"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/DeveloperToolsPage.qml:34
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/DeveloperToolsPage.qml:34
msgid "Install app or watchface from file"
msgstr "Instalar app ou watchface de arquivo"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:6
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:99
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:6
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:85
msgid "Firmware upgrade"
msgstr "Atualização do firmware"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:16
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:16
msgid "A new firmware upgrade is available for your Pebble smartwatch."
msgstr ""
"Um novo upgrade de firmware está disponível para o seu Pebble smartwatch."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:23
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:23
#, qt-format
msgid "Currently installed firmware: %1"
msgstr "Firmware instalado atualmente: %1"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:29
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:29
#, qt-format
msgid "Candidate firmware version: %1"
msgstr "Versão de firmaware candidata: %1"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:35
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:35
#, qt-format
msgid "Release Notes: %1"
msgstr "Notas da versão: %1"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:41
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:41
msgid "Important:"
msgstr "Importante:"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/FirmwareUpgradePage.qml:41
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/FirmwareUpgradePage.qml:41
msgid ""
"This update will also upgrade recovery data. Make sure your Pebble "
"smartwarch is connected to a power adapter."
msgstr ""
"Esta atualização também irá atualizar os dados de recuperação . Verifique se "
"o seu smartwarch Pebble está conectado a um adaptador de energia."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthInfoItem.qml:224
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthInfoItem.qml:224
msgid "KM"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthInfoItem.qml:263
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthInfoItem.qml:263
msgid "M"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthPage.qml:7
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthPage.qml:7
#, fuzzy
msgid "Health info"
msgstr "Configurações do Health"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:9
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:9
msgid "Health settings"
msgstr "Configurações do Health"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:17
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:17
msgid "Health app enabled"
msgstr "Aplicação Health ativada"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:28
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:28
msgid "Female"
msgstr "Feminino"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:28
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:28
msgid "Male"
msgstr "Masculino"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:34
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:34
msgid "Age"
msgstr "Idade"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:47
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:47
msgid "Height (cm)"
msgstr "Altura (cm)"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:60
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:60
msgid "Weight"
msgstr "Peso"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:73
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:73
msgid "I want to be more active"
msgstr "Eu quero ser mais ativo"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:84
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:84
msgid "I want to sleep more"
msgstr "Eu quero dormir mais"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:95
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/AlertDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/ConfirmDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/PromptDialog.qml:14
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:95
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/AlertDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/ConfirmDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/PromptDialog.qml:14
msgid "OK"
msgstr "OK"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/HealthSettingsDialog.qml:110
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:99
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:55
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/ConfirmDialog.qml:13
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/PromptDialog.qml:20
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/HealthSettingsDialog.qml:110
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:99
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:55
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/ConfirmDialog.qml:13
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/PromptDialog.qml:20
msgid "Cancel"
msgstr "Cancelar"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ImportPackagePage.qml:7
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ImportPackagePage.qml:7
msgid "Import watchapp or watchface"
msgstr "Importar watchapp ou watchface"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InfoPage.qml:35
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InfoPage.qml:35
#, qt-format
msgid "Version %1"
msgstr "Versão %1"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InfoPage.qml:45
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InfoPage.qml:45
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:8
msgid "Report problem"
msgstr "Reportar problema"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InfoPage.qml:56
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InfoPage.qml:56
msgid "Legal"
msgstr "Legal"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InfoPage.qml:78
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InfoPage.qml:78
msgid ""
"This application is neither affiliated with nor endorsed by Pebble "
"Technology Corp."
msgstr ""
"Esta aplicação não é afiliada nem endossada pela Pebble Technology Corp."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InfoPage.qml:83
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InfoPage.qml:83
msgid "Pebble is a trademark of Pebble Technology Corp."
msgstr "Pebble é uma marca comercial da Pebble Technology Corp."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:9
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:9
msgid "Apps & Watchfaces"
msgstr "Apps e Watchfaces"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:9
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:9
msgid "Apps"
msgstr "Apps"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:9
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:9
msgid "Watchfaces"
msgstr "Watchfaces"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:167
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:167
msgid "Launch"
msgstr "Iniciar"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:177
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:176
msgid "Configure"
msgstr "Configurar"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/InstalledAppsPage.qml:187
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:91
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/InstalledAppsPage.qml:186
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:91
msgid "Delete"
msgstr "Eliminar"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/LoadingPage.qml:42
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/Main.qml:95
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/LoadingPage.qml:42
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/Main.qml:95
msgid "Loading..."
msgstr "A carregar..."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:15
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:15
msgid "About"
msgstr "Acerca"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:22
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:22
msgid "Developer tools"
msgstr "Ferramentas do programador"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:53
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:53
msgid "Manage notifications"
msgstr "Gerir notificações"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:60
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:60
msgid "Manage Apps"
msgstr "Gerir apps"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:69
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:68
msgid "Manage Watchfaces"
msgstr "Gerir watchfaces"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:89
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:76
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:8
msgid "Settings"
msgstr "Configurações"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:205
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/PebblesPage.qml:36
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:191
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/PebblesPage.qml:36
msgid "Connected"
msgstr "Ligado"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:205
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/PebblesPage.qml:36
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:191
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/PebblesPage.qml:36
msgid "Disconnected"
msgstr "Desconectado"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:218
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:204
msgid ""
"Your Pebble smartwatch is disconnected. Please make sure it is powered on, "
"within range and it is paired properly in the Bluetooth System Settings."
msgstr ""
"O seu smartwatch Pebble está desconectado. Por favor, verifique se ele está "
"ligado, dentro do alcance e emparelhado corretamente nas configurações do "
"Bluetooth."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:228
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/PebblesPage.qml:64
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:214
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/PebblesPage.qml:64
msgid "Open System Settings"
msgstr "Abra as Definições de Sistema"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:236
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:222
msgid "Your Pebble smartwatch is in factory mode and needs to be initialized."
msgstr ""
"O seu smartwatch Pebble está no modo de fábrica e precisa ser inicializado."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:245
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:231
msgid "Initialize Pebble"
msgstr "Inicializar o Pebble"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MainMenuPage.qml:277
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MainMenuPage.qml:263
msgid "Upgrading..."
msgstr "A actualizar..."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MonthlyStepsGraph.qml:120
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/WeeklyStepsGraph.qml:108
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MonthlyStepsGraph.qml:120
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/WeeklyStepsGraph.qml:108
msgid "AVERAGE STEPS"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/MonthlyStepsGraph.qml:120
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/WeeklyStepsGraph.qml:108
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/MonthlyStepsGraph.qml:120
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/WeeklyStepsGraph.qml:108
#, qt-format
msgid "TYPICAL %1"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/NotificationsPage.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/NotificationsPage.qml:8
msgid "Notifications"
msgstr "Notificações"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/NotificationsPage.qml:29
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/NotificationsPage.qml:29
msgid ""
"Entries here will be added as notifications appear on the phone. Selected "
"notifications will be shown on your Pebble smartwatch."
msgstr ""
"As entradas aqui vão ser adicionadas como notificações no telefone. As "
"notificações seleccionadas serão exibidas no seu smartwatch Pebble."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/PebblesPage.qml:6
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/PebblesPage.qml:6
msgid "Manage Pebble Watches"
msgstr "Gerir relógios Pebble"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/PebblesPage.qml:57
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/PebblesPage.qml:57
msgid ""
"No Pebble smartwatches configured yet. Please connect your Pebble smartwatch "
"using System Settings."
msgstr ""
"Não existem smartwatches Pebble configurados ainda. Por favor, ligue o seu "
"smartwatch Pebble através de Configurações do sistema."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:69
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:69
msgid "Screenshot options"
msgstr "Opções de capturas do ecrã"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:74
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:74
msgid "Share"
msgstr "Partilhar"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:77
#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:85
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:77
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:85
msgid "Pebble screenshot"
msgstr "Capturas de ecrã Pebble"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/ScreenshotsPage.qml:82
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/ScreenshotsPage.qml:82
msgid "Save"
msgstr "Guardar"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:18
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:18
msgid "Preparing logs package..."
msgstr "A preparar pacote de logs..."

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:29
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:29
msgid "pebble.log"
msgstr "pebble.log"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:36
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:36
msgid "Send rockworkd.log"
msgstr "Enviar rockworkd.log"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:41
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:41
msgid "rockworkd.log"
msgstr "rockworkd.log"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SendLogsDialog.qml:46
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SendLogsDialog.qml:46
msgid "Send watch logs"
msgstr "Enviar logs do relógio"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:19
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:19
msgid "Distance Units"
msgstr "Unidades de distância"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:35
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:35
msgid "Metric"
msgstr "Métrica"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:48
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:48
msgid "Imperial"
msgstr "Imperial"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:55
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:55
msgid "Calendar"
msgstr "Calendário"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/SettingsPage.qml:62
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/SettingsPage.qml:62
msgid "Sync calendar to timeline"
msgstr "Sincronizar calendário para o timeline"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/WeeklySleepGraph.qml:154
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/WeeklySleepGraph.qml:154
msgid "AVERAGE SLEEP"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/WeeklySleepGraph.qml:154
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/WeeklySleepGraph.qml:154
#, qt-format
msgid "TYPICAL %1H %2M"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/AlertDialog.qml:5
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/AlertDialog.qml:5
#, fuzzy
msgid "App Settings Alert"
msgstr "Definições"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/BeforeUnloadDialog.qml:5
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/BeforeUnloadDialog.qml:5
msgid "App Settings Confirm Navigation"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/BeforeUnloadDialog.qml:8
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/BeforeUnloadDialog.qml:8
msgid "Leave"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/BeforeUnloadDialog.qml:13
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/BeforeUnloadDialog.qml:13
msgid "Stay"
msgstr ""

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/ConfirmDialog.qml:5
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/ConfirmDialog.qml:5
#, fuzzy
msgid "App Settings Confirmation"
msgstr "Definições"

#: /home/brian/dev/ubuntu/apps/rockwork/rockwork/js-dialogs/PromptDialog.qml:5
#: /home/brian/dev/ubuntu/apps/rockwork/archive/trunk/rockwork/js-dialogs/PromptDialog.qml:5
#, fuzzy
msgid "App Settings Prompt"
msgstr "Definições"

#, fuzzy
#~ msgid "Health"
#~ msgstr "Configurações do Health"
